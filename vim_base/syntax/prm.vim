" Vim syntax file
" Language:         prm  File
" Maintainer:       Nikolai Weibull <now@bitwi.se>
" Latest Revision:  2006-04-19

if exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

syn keyword prmvartype     float string wfile rfile int flag short  global
syn keyword prmstructure   structure member paramgrp variable
syn keyword prmvarfield    type s_desc default min unit max units c_exp menu l_desc ext
syn keyword prmvarfield    auxiliary hidden
syn keyword prmglobal      trail_args gallocation inc_code view

syn region  prmComment     start='#' end='$' 
syn region  prmString      start='."'hs=s+1 end='"' 

" Floating point number with decimal no E or e (+,-)
syn match prmNumber '[-+]\=\d\+\.\=\d*'
" Floating point like number with E and no decimal point (+,-)
syn match prmNumber '[-+]\=\d[[:digit:]]*[eE][\-+]\=\d\+' 
syn match prmNumber '\d[[:digit:]]*[eE][\-+]\=\d\+' 
" Floating point like number with E and decimal point (+,-)
syn match prmNumber '[-+]\=\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+' 
syn match prmNumber '\d[[:digit:]]*\.\d*[eE][\-+]\=[\d-]\+' 

" options themselves
syn region prmOption     start='\$member\s'hs=s+8 end='{'he=e-1 contains=prmstructure
syn region prmOption     start='\$variable\s'hs=s+10 end='{'he=e-1 contains=prmstructure

hi def link prmString      String
hi def link prmComment     Comment
hi def link prmvartype     Type
hi def link prmstructure   Statement
hi def link prmglobal      Statement
hi def link prmvarfield    Label
hi def link prmNumber      Constant
hi def link prmOption      Identifier

let b:current_syntax = "prm"

let &cpo = s:cpo_save
unlet s:cpo_save
