" Vim syntax file
" Language:         gengetopt  File
" Maintainer:       Edward Vigmond <ed.vigmond@gmail.com>
" Latest Revision:  2012-07-26

if exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&


syn keyword ggocommand     package version purpose usage description arg  
syn keyword ggocommand     section text args defmode defgroup

syn keyword ggoopts        option modeoption groupoption  skipwhite nextgroup=ggoLongopt

syn keyword ggooptarg      long short details typestr values default dependon 
syn keyword ggooptarg      required optional argoptional multiple hidden flag modedesc
syn keyword ggooptarg      groupdesc on off enum yes no mode group
syn keyword ggooptarg      string int short long float double longdouble longlong

syn region  ggoComment     start='#' end='$' 

syn match   ggoLongopt    '"[a-zA-Z0-9.-]\+"' nextgroup=ggoShortopt 
syn match   ggoShortopt   '\s[a-zA-Z0-9-]\s' 

syn region  ggodesc       start='"' end='"' 

syn region  ggoString      start='="'hs=s+1 end='"' 

" Floating point number with decimal no E or e (+,-)
syn match ggoNumber '"\d\+"' 
syn match ggoNumber '\d\+\.\d*' 
syn match ggoNumber '[-+]\d\+\.\d*'
" Floating point like number with E and no decimal point (+,-)
syn match ggoNumber '[-+]\=\d[[:digit:]]*[eE][\-+]\=\d\+' 
syn match ggoNumber '\d[[:digit:]]*[eE][\-+]\=\d\+' 
" Floating point like number with E and decimal point (+,-)
syn match ggoNumber '[-+]\=\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+' 
syn match ggoNumber '\d[[:digit:]]*\.\d*[eE][\-+]\=[\d-]\+' 

let b:current_syntax = "gengetopt"

hi def link ggoopts        PreProc
hi def link ggocommand     PreProc
hi def link ggoLongopt     Type
hi def link ggoShortopt    Type
hi def link ggooptarg      Label
hi def link ggoNumber      Constant
hi def link ggoString      Constant
hi def link ggoComment     Comment

let &cpo = s:cpo_save
unlet s:cpo_save
