" Vim syntax file
" Language:         LIMPET model  File. This is an extension of C
" Maintainer:       Edward Vigmond
" Latest Revision:  2010-12-15

if exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

runtime! syntax/c.vim
unlet b:current_syntax

"EasyML model stuff is below here
syn keyword Time      dt
syn keyword Time      t

syn keyword intmethod fe rk2 rk4 sundnes rush_larsen rosenbrock cvode markov_be contained
syn keyword EMLunits  contained unitless
syn match   EMLunits  contained '\<[fpnumckMGT]\?[mMLsSFVCAJNK]\>'
syn match   EMLunits  contained '[fpnumkMGT]\?mol' 
syn keyword EMLunits  group 
syn match   EMLunits '@' 

syn region EMLaction   start='\.method(' end=')' contains=intmethod 
syn match  EMLaction   '\.store' 
syn match  EMLaction   '\.param' 
syn match  EMLaction   '\.external' 
syn match  EMLaction   '\.regional' 
syn match  EMLaction   '\.nodal'
syn match  EMLaction   '\.lookup' 
syn match  EMLaction   '\.ub' 
syn match  EMLaction   '\.lb' 
syn region EMLunitSpec   start='\.units(' end=')' contains=EMLunits 
syn match  EMLaction   '\.trace' 
syn match  EMLaction   '\.array'
syn match  EMLaction   '\.external_function' 

syn match   modvartype   '\<a_\w\+' 
syn match   modvartype   '\<b_\w\+' 
syn match   modvartype   '\<alpha_\w\+' 
syn match   modvartype   '\<beta_\w\+' 
syn match   modvartype   '\<tau_\w\+' 
syn match   modvartype   '^diff_\w\+' 
syn match   modvartype   '\<diff_\w\+' 
syn match   modvartype   '\<d_\w\+_dt' 
syn match   modvartype   '\w\+_inf' 
syn match   modvartype   '\w\+_init' 
syn match   modvartype   '\>trace_\w\+' 

syn match   cComment     '#.*$'

syn keyword cConditional   elif 
"
hi def link modvartype     Type
hi def link EMLaction      Identifier
hi def link EMLunitSpec    Identifier
hi def link EMLunits       PreProc
hi def link intmethod      PreProc
hi def link Time           Type

let b:current_syntax = "EasyML"

let &cpo = s:cpo_save
unlet s:cpo_save

