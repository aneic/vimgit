VIM git repo of Aurel Neic
==========================

This is the git repository for my vim configuration.


Usage
-----

To use this configuration, generate soft links to vimrc and vim\_base:

    cd
    ln -s /path/to/vimrc .vimrc
    ln -s /path/to/vim_base .vim