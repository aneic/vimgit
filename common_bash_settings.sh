# Aliases
alias pu='ps -u $USER'
alias ls='/usr/bin/ls --color=auto --group-directories-first'
alias ll='ls -lh'
alias lbk='. load_bookmark.sh'
alias dotags='ctags --exclude=*.tex --exclude=*.py --exclude=*.js --c++-kinds=+f \
--c-kinds=+p --fields=+imaSftn --extra=+q --langmap=c++:+.cu $(find . -name \*.cc -o \
-name \*.c -o -name \*.h -o -name \*.cpp -o -name \*.cxx -o -name \*.hpp -o -name \*.hxx)'

alias gitk='/usr/bin/gitk --all'
alias gitc='git log --decorate --pretty=short --graph --all'
alias gith='git rev-parse HEAD'
alias codesearch='strsearch c'
alias fix_paste='printf "\e[?2004l"'
# git commits can be borwsed with the following alias.
# use K to jump into a specific commit (use the language specific manual)
alias browsegit='git log --name-status | vim -R -'


# this is a fix to bash-completition
_completion_loader () {
  local dir=/usr/share/bash-completion/completions
  local cmd="${1##*/}"
  . "${dir}/${cmd}" &>/dev/null && return 124
  complete -o default -o bashdefault "${cmd}" &>/dev/null && return 124
}


