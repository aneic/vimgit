" general stuff
" syntax completition
syntax on
set lazyredraw
" backspace over everything
set backspace=2
" show line numbers
set number
"set relativenumber
" scroll offset
set scrolloff=0
set hlsearch
set noincsearch
" tabbing
set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set wildmode=longest,list
" mouse support
set mouse=a
" status line
set laststatus=2
" recursive path
set path=**
" indent lines when line-breaking
" set breakindent
" show special characters
set list
set listchars=tab:>-,trail:+,nbsp:X

" some syntax settings
" gnuplot
autocmd BufNewFile,BufRead *.gp setlocal filetype=gnuplot
"prm files
au  BufRead,BufNewFile *.prm set filetype=prm
"au! Syntax prm source /home/aneic/.vim/syntax/prm.vim
"IMPmodel
au  BufRead,BufNewFile *.model set filetype=EasyML
"au! Syntax EasyML source /home/aneic/.vim/syntax/EasyML.vim
"gengetopt
au  BufRead,BufNewFile *.ggo set filetype=gengetopt
"au! Syntax gengetopt source /home/aneic/.vim/syntax/gengetopt.vim
".def files
au  BufRead,BufNewFile *.def set filetype=make


" reduce some rendering load for latex
autocmd FileType tex :NoMatchParen
autocmd FileType tex setlocal nocursorline


"set guifont=Source\ Code\ Pro\ Medium\ 10
"set guifont=DejaVu\ Sans\ Mono\ 10

" specific changes to colorscheme based on gui or term usage
if has("gui_running")
  " set guifont=Input\ Regular\ 10
  " set guifont=Latin\ Modern\ Mono\ Regular\ 10
  "set guifont=Source\ Code\ Pro\ Medium\ 10
  set guifont=Fira\ Mono
  set background=dark
  colorscheme nova
else
  set background=light
  colorscheme PaperColor
endif

set cursorline

" set tgc
" colorscheme nova
" set background=light
" set cursorline
" set cursorcolumn


" how to highlight too long lines:
" either text-background
" match OverLength /\%91v.\+/
" or with a colorcolumn
set colorcolumn=90

" Function definitions ===============================================
" functions to highlight tabs, and trailing white spaces
" highlight ExtraWhitespace ctermbg=lightred guibg=lightred
" highlight Tabs ctermbg=lightgreen guibg=lightgreen
" Match spetial characters (not needed because list does a better job)
" function MatchTab()
" match Tabs /\t/
" endfunction
" function MatchTrailSpace()
" match ExtraWhitespace /\s\+$/
" endfunction
" function NoMatch()
" match
" endfunction

function ToggleCopyPaste()
  if &mouse == 'a'
    set nonumber
    set mouse=""
    set noautoindent
    set formatoptions-=cro
  else
    set number
    set mouse=a
    set autoindent
    set formatoptions+=cro
  endif
endfunction

function ToggleExpandTab()
  set expandtab!
  set expandtab?
endfunction

function ToggleDarkMode()
  if &background == 'dark'
    set background=light
    "colorscheme PaperColor
  else
    set background=dark
    "colorscheme nova
  endif
endfunction

" function and command definitions for removing trailing spaces
function RemTrailSpaces()
:%s/\s\+$//
endfunction
command! RemTrailSpaces call RemTrailSpaces()

function FixSpaces()
:.s/ *, */, /ge
:.s/( /(/ge
:.s/ )/)/ge
endfunction
command! FixSpaces call FixSpaces()

"command CPToggle :call ToggleCopyPaste()
"command CopyPastOff :call SetCPOff()

" by default line indent highlight is disabled
" it can be enabled with IndentLinesToggle
let g:indentLine_enabled = 0

" key mappings ===================================================
" moving text
vmap <F2> :m '<-2<CR>gv=gv
vmap <F3> :m '>+1<CR>gv=gv
" easier copy-paste in terminal sessions
if has("gui_running")
  nmap <F2> :IndentLinesToggle<CR>
else
  nmap <F2> :call ToggleCopyPaste()<CR>
endif

" indent with space or tabs
nmap <F3> :call ToggleExpandTab()<CR>
" insert Doxygen header
nmap <F4> :Dox<CR>
" in visual mode, generate table from selection
vmap <F4> !column -t<CR>gv=
" remapping of copy and paste
vmap <F5> "+y
nmap <F6> "+gP
" Tagbar
nmap <F8> :TagbarToggle<CR>

" configure statusline
set statusline=
set statusline+=\ %f\ :\ %l\ :\ %v\ %0.m%0.r%0.y
" this is how to print the absolute path of the current file
"set statusline+=\ (%<%.80{expand('%:p:h')})\ 
set statusline+=%=
set statusline+=%{strftime('%H:%M')}\ --
set statusline+=\ %3.3p\ %%\ 

" disable search for latex errors
let g:tex_no_error=1
